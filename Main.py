import time
import board
import busio
import digitalio
import pwmio
import audiocore
import audiobusio

from adafruit_motor import servo
from adafruit_mcp230xx.mcp23017 import MCP23017
from analogio import AnalogIn

# ###################################################
# Written by: Tim Wappat
#             https://timwappat.info
# Ghost trap code for Raspberry Pi Pico
# Written: October 2021
# Activate doors by press of footpedal or side button
# Doors open with optional smoke
# Doors close
# Trapped ghost mode where beeps and flashes
# Trapped ghost mode times out or can be stopped with another press
# Press and hold side button for two seconds to allow smoke or not allow smoke
# Toggle switch disables sound effects and opening doors - used for when in belt holder
#
#
# ###################################################

# Sound effects on or off
sfxOn = True
# Smoke effect on or off
smokeEffectOn = False
footPedalState = True
minVoltage = 0
maxVoltage = 0

# ------------- Define in and outs --------------------------------------------------------------------
# Define the rotary encoder push button on side of trap
sidebutton = digitalio.DigitalInOut(board.GP22)
sidebutton.switch_to_input(pull=digitalio.Pull.UP)

# Define the top toggle switch on GP11 - used to put trap into restricted mode if in holster (no doors but smoke)
top_toggle_switch = digitalio.DigitalInOut(board.GP11)
top_toggle_switch.switch_to_input(pull=digitalio.Pull.UP)

# Define Analog pin A1 to measure the volts at the pedal LED (pedal breaks circuit causing to go high)
pedal_LED_analog_levelpin = AnalogIn(board.A1)
# Define GP8 for driving the LED in foot pedal - also used to provide voltage
# detected by analog pin for activation and pedal disconnected
footpedal_LED_drive = digitalio.DigitalInOut(board.GP8)
footpedal_LED_drive.switch_to_output(value=True)

# Define analog pin to detect the system voltage (not used)
vsys = AnalogIn(board.A3)

# Variables for keeping track of is_pedalClosed time events happened
NextFootPedalStateChange = time.monotonic() + 1
is_pedalClosed_foot_pedal_went_low = time.monotonic()

# Define GP7 for driving the smoke pump motor
smoke_pump_motor = digitalio.DigitalInOut(board.GP7)
smoke_pump_motor.switch_to_output(value=False)

# Define GP6 for blinder white LEDS shine out of trap
led_blinder_trap_leds = digitalio.DigitalInOut(board.GP6)
led_blinder_trap_leds.switch_to_output(value=False)

# Define the i2c bus that is used for various display LEDs via MCP23017
i2c = busio.I2C(board.GP5, board.GP4)
mcp = MCP23017(i2c, address=0x20)
# Map MCP23017 pins to software pins
pin0 = mcp.get_pin(0)
pin1 = mcp.get_pin(1)
pin2 = mcp.get_pin(2)
pin3 = mcp.get_pin(3)
pin4 = mcp.get_pin(4)
pin5 = mcp.get_pin(5)
pin6 = mcp.get_pin(11)
pin7 = mcp.get_pin(12)
pin8 = mcp.get_pin(13)
pin9 = mcp.get_pin(14)
pin10 = mcp.get_pin(15)
pin11 = mcp.get_pin(6)
# Set default state for MCP pins
pin0.switch_to_output(value=True)
pin1.switch_to_output(value=True)
pin2.switch_to_output(value=True)
pin3.switch_to_output(value=True)
pin4.switch_to_output(value=True)
pin5.switch_to_output(value=True)
pin6.switch_to_output(value=True)
pin7.switch_to_output(value=True)
pin8.switch_to_output(value=True)
pin9.switch_to_output(value=True)
pin10.switch_to_output(value=True)
pin11.switch_to_output(value=False)

# ------------ set up audio effects ---------------------------
# Define the i2s bus - for driving audio board
i2s = audiobusio.I2SOut(board.GP13, board.GP14, board.GP15)
# Pre-open audio files for quick replay
file_trap_beep_sfx = open("trapbeep.wav", "rb")
wav_file_trap_beep = audiocore.WaveFile(file_trap_beep_sfx)

file_trap_close_sfx = open("trapclose1.wav", "rb")
wav_doors_close = audiocore.WaveFile(file_trap_close_sfx)

file_no_ghost_sfx = open("noghost.wav", "rb")
wav_no_ghost = audiocore.WaveFile(file_no_ghost_sfx)

# Every three mins bleep so don't forget its on
next_wake_display_time = time.monotonic() + 120

# Measure voltage at the pedal LED, if floating as open circuit will be high (footpedal active)
def is_pedalClosed(footpedal):
    global maxVoltage
    global minVoltage
    global maxVoltageLowerCount
    # let voltage settle before taking a reading
    time.sleep(0.1)
    measuredVoltage = getFootPedalVoltage(footpedal)
    print(
        "Measured:"
        + str(measuredVoltage)
        + "   Max:"
        + str(maxVoltage)
        + "   Min:"
        + str(minVoltage)
        + "  Vsys:" + str(vsys.value * 3.23 * 3 / 65535)
    )
    # if zero then just set as inital volts
    if minVoltage == 0 or maxVoltage == 0:
        minVoltage = measuredVoltage
        maxVoltage = measuredVoltage
        maxVoltageLowerCount = 0
        return True
    else:
        if measuredVoltage < minVoltage:
            minVoltage = measuredVoltage
        if measuredVoltage > maxVoltage:
            maxVoltage = measuredVoltage
            maxVoltageLowerCount = 0
        # 2.7
        if measuredVoltage < maxVoltage-0.2:
            print("foot false")
            return False
        else:
            print("foot true")
            return True


def getFootPedalVoltage(footpedal):
    valuea = footpedal.value * (3.3 / 65535) * 3.3
    time.sleep(0.005)
    valueb = footpedal.value * (3.3 / 65535) * 3.3
    time.sleep(0.005)
    valuec = footpedal.value * (3.3 / 65535) * 3.3
    return (valuea + valueb + valuec) / 3


def doors(state):
    # create a PWMOut object on Pin A2.
    if state is True:
        print("Open")
        door_servoLeft.angle = 0
        time.sleep(0.04)
        door_servoRight.angle = 140
    else:
        print("Close")
        door_servoLeft.angle = 160
        time.sleep(0.08)
        door_servoRight.angle = 0


def playEffectTrappedBeep():
    if sfxOn is True:
        i2s.play(wav_file_trap_beep)
        while i2s.playing:
            time.sleep(0.1)
    else:
        time.sleep(0.15)


def playEffectdoorsopen():
    if sfxOn is True:
        i2s.play(wav_doors_close)
        while i2s.playing:
            time.sleep(0.05)
        i2s.play(wav_no_ghost)
        time.sleep(1.1)
        smoke_pump_motor.value = False
        doors(False)
        while i2s.playing:
            time.sleep(0.05)
    else:
        time.sleep(3)
        doors(False)


def resetLeds():
    print("reset leds")
    pin0.value = True
    pin1.value = True
    pin2.value = True
    pin3.value = True
    pin4.value = True
    pin5.value = True
    pin6.value = True
    pin7.value = True
    pin8.value = True
    pin9.value = True
    pin10.value = True
    pin11.value = False


def openDoorsSequence():
    doors(True)
    time.sleep(0.5)
    playEffectdoorsopen()

    doors(False)
    time.sleep(1)
    BarInterBarDelay = 0.1

    pin0.value = False
    time.sleep(BarInterBarDelay)
    pin1.value = False
    time.sleep(BarInterBarDelay)
    pin2.value = False
    time.sleep(BarInterBarDelay)
    pin3.value = False
    time.sleep(BarInterBarDelay)
    pin4.value = False
    time.sleep(BarInterBarDelay)

    door_servoLeft.angle = None
    door_servoRight.angle = None

    pin5.value = False
    time.sleep(BarInterBarDelay)
    pin6.value = False
    time.sleep(BarInterBarDelay)
    pin7.value = False
    time.sleep(BarInterBarDelay)
    pin8.value = False
    time.sleep(BarInterBarDelay)
    pin9.value = False
    time.sleep(BarInterBarDelay + 0.05)
    pin10.value = False
    time.sleep(1)


def trapped(pedal_LED_analog_levelpin):
    loopcounter = 0
    # Now loop blinking the pin 0 output and reading the state of pin 1 input.
    exttrappedstatechange = time.monotonic() + 0.08
    while loopcounter < 3000 and sidebutton.value:
        footpedalstate_b = is_pedalClosed(pedal_LED_analog_levelpin)
        print(str(footpedal_LED_drive.value) + " " + str(footpedalstate_b) + " ")
        if footpedal_LED_drive.value and (not footpedalstate_b):
            loopcounter = 3000
            print("exit loop counter forced 3000")
        if time.monotonic() > exttrappedstatechange:
            pin11.value = not pin11.value
            if pin11.value:
                playEffectTrappedBeep()
                exttrappedstatechange = time.monotonic() + 0.2
                loopcounter += 1
            else:
                exttrappedstatechange = time.monotonic() + 0.2
                loopcounter += 1
        print(loopcounter)


def KeepFootPedalFlashing(
    NextFootPedalStateChange,
    footPedalState,
):
    # print(NextFootPedalStateChange,  time.monotonic())
    if NextFootPedalStateChange < time.monotonic():
        footpedal_LED_drive.value = not footpedal_LED_drive.value
        footPedalState = not footPedalState
        NextFootPedalStateChange = time.monotonic() + 0.1

    return (
        NextFootPedalStateChange,
        footPedalState,
    )


# Does LED sequence and a beep to say its alive on power up and when left alone
def WakeDisplayTest():
    BarInterBarDelay = 0.125
    # beep
    i2s.play(wav_file_trap_beep)
    pin0.value = False
    pin10.value = False
    time.sleep(BarInterBarDelay)
    pin1.value = False
    pin9.value = False
    pin0.value = True
    pin10.value = True
    time.sleep(BarInterBarDelay)
    pin2.value = False
    pin8.value = False
    pin1.value = True
    pin9.value = True
    time.sleep(BarInterBarDelay)
    pin3.value = False
    pin7.value = False
    pin2.value = True
    pin8.value = True
    time.sleep(BarInterBarDelay)
    pin4.value = False
    pin6.value = False
    pin3.value = True
    pin7.value = True
    time.sleep(BarInterBarDelay)
    pin5.value = False
    pin4.value = True
    pin6.value = True
    time.sleep(BarInterBarDelay)
    pin5.value = True
    while i2s.playing:
        time.sleep(0.1)


def SmokeEffectModeSelection(state):
    if (state is True):
        pin0.value = False
        pin1.value = False
        pin2.value = False
        pin3.value = False
        pin4.value = False
        pin5.value = False
        pin6.value = False
        pin7.value = False
        pin8.value = False
        pin9.value = False
        time.sleep(0.7)
        pin0.value = True
        pin1.value = True
        pin2.value = True
        pin3.value = True
        pin4.value = True
        pin5.value = True
        pin6.value = True
        pin7.value = True
        pin8.value = True
        pin9.value = True
    else:
        pin0.value = False
        pin1.value = False
        pin2.value = True
        pin3.value = True
        pin4.value = True
        pin5.value = True
        pin6.value = True
        pin7.value = True
        pin8.value = False
        pin9.value = False
        time.sleep(0.8)
        pin0.value = True
        pin1.value = True
        pin2.value = True
        pin3.value = True
        pin4.value = True
        pin5.value = True
        pin6.value = True
        pin7.value = True
        pin8.value = True
        pin9.value = True

def doMainSeq(pedal_LED_analog_levelpin) :
    print("Main sequence activated")

    if smokeEffectOn:
        smoke_pump_motor.value = True
        time.sleep(1)
    # if restricted toggle switch on, don't open doors (in holster?)
    if top_toggle_switch.value:
        print("Use doors")
        led_blinder_trap_leds.value = True
        openDoorsSequence()
        trapped(pedal_LED_analog_levelpin)
        led_blinder_trap_leds.value = False
        resetLeds()
        time.sleep(2)
    else:
        print("Don't use doors")
        time.sleep(3)
        smoke_pump_motor.value = False

# Set up door servos close the doors and power the servos down again
pwm1 = pwmio.PWMOut(board.GP21, duty_cycle=2 ** 15, frequency=50)
pwm2 = pwmio.PWMOut(board.GP20, duty_cycle=2 ** 15, frequency=50)
door_servoRight = servo.Servo(pwm1)
door_servoLeft = servo.Servo(pwm2)
# door_servoRight.angle = 0
doors(False)
time.sleep(1)
door_servoLeft.angle = None
door_servoRight.angle = None

# Power up test with leds and beep
WakeDisplayTest()

# Main loop sarts
while True:
    # Flash foot pedal
    (
        NextFootPedalStateChange,
        footPedalState,
    ) = KeepFootPedalFlashing(
        NextFootPedalStateChange,
        footPedalState,
    )
    # if safety switch is set only do smoke effect, only allow footpedal to activate if its deemed to be attached
    if not (sidebutton.value) :
        print("Side button detected")
        # if button held for 1.5 secs then toggle smoke on/off
        sidebuttonTimer = time.monotonic()
        while not (sidebutton.value) and ((sidebuttonTimer + 1.5) > time.monotonic()) :
            time.sleep(0.001)

        if (sidebuttonTimer + 1.5) > time.monotonic():
            # do open sequence
            # physical interaction, so push sleep timer into future again
            print("side button action")
            next_wake_display_time = time.monotonic() + 120
            doMainSeq(pedal_LED_analog_levelpin)
            # Reset upper voltage to deal with footpedal getting plugged in again
            maxVoltage = 0
        else :
            # toggle the smoke
            print("Toggled smoke effect")
            smokeEffectOn = not smokeEffectOn
            SmokeEffectModeSelection(smokeEffectOn)

    else :
        if (footPedalState and (not is_pedalClosed(pedal_LED_analog_levelpin))):
            # do open sequence
            # physical interaction, so push sleep timer into future again
            print("Footpedal detected")
            next_wake_display_time = time.monotonic() + 120
            doMainSeq(pedal_LED_analog_levelpin)
            # Reset upper voltage to deal with footpedal getting plugged in again
            maxVoltage = 0
    # do the startup test every 2 mins to remind user trap is powered on, hope ot prevent battery loss
    if next_wake_display_time < time.monotonic():
        WakeDisplayTest()
        next_wake_display_time = time.monotonic() + 120
    time.sleep(0.01)

